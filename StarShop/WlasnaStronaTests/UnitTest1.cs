﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using WlasnaStrona.Models;
using WlasnaStrona.Controllers;
using Moq;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.EntityModel;

namespace WlasnaStronaTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestLogowania()
        {
            //przygotowanie

            Mock<IRepository> listLoggins = new Mock<IRepository>();
            listLoggins.Setup(x => x.DaneTablicowe).Returns(new List<Table>{new Table{Id=1, Login="Arkadiusz",Password="crysis"},
                new Table{Id=2,Login="Arkadiusz",Password="321321"},
                new Table{Id=3,Login="1",Password="1"}});

            Mock<IStarRepository> listStar = new Mock<IStarRepository>();
            listStar.Setup(x => x.SpisGwiazd).Returns(new List<Stars>{new Stars{Id=1,Nazwa="Gwiazda1",Opis="Opis",ImgLink="Link",Rezerwacja=null},
                new Stars{Id=2,Nazwa="Gwiazda2",Opis="Opis",ImgLink="Link",Rezerwacja=null}});

            HomeController kontroler = new HomeController(listLoggins.Object,listStar.Object);

            Klasa OsobaFalse = new Klasa { Login = "Adiusz", Password = "1122" };
    
            //działanie

            var resultF = kontroler.Starts(OsobaFalse) as ViewResult;
            
            //asercje

            Assert.AreEqual(null, resultF.Model);
            
        }

        [TestMethod]
        public void TestujRejestracje()
        {
            //przygotowanie
            Mock<IRepository> bazaPerson = new Mock<IRepository>();

            bazaPerson.Setup(x=>x.DaneTablicowe).Returns(new List<Table>{new Table{Id=1,Login="User1",Password="Password1"},
                new Table{Id=2,Login="User2",Password="Password2"},
                new Table{Id=0,Login="Admin", Password="Admin"}});

            Mock<IStarRepository> listStar = new Mock<IStarRepository>();
            listStar.Setup(x => x.SpisGwiazd).Returns(new List<Stars>{new Stars{Id=1,Nazwa="Gwiazda1",Opis="Opis",ImgLink="Link",Rezerwacja=null},
                new Stars{Id=2,Nazwa="Gwiazda2",Opis="Opis",ImgLink="Link",Rezerwacja=null}});

            Personalia NewUserRepeatLoggin = new Personalia { Login = "User1", Password = "Password", RepeatPassword = "Password" };
            Personalia NewUserOtherLoggin = new Personalia { Login = "NewUser", Password = "Password", RepeatPassword = "Password" };

            HomeController kontroler = new HomeController(bazaPerson.Object, listStar.Object);

            //działanie
            var result = kontroler.Rejestracja(NewUserRepeatLoggin) as ViewResult;
            var resultTrue = kontroler.Rejestracja(NewUserOtherLoggin) as ViewResult;
          

            //asercje

            Assert.AreEqual(null, result.Model);
            Assert.AreEqual("Result", resultTrue.ViewName);
        }
    }
}
