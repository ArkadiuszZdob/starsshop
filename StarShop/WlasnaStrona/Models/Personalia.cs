﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WlasnaStrona.Models
{
    public class Personalia
    {
        [Required(ErrorMessage = "Podaj prawidłowy login")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Podaj prawidłowe hasło")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Podaj prawidłowe hasło")]
        public string RepeatPassword { get; set; }

    }
}