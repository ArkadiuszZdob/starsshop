﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WlasnaStrona.Infrastructure;
using WlasnaStrona.Controllers;
using System.Data.Entity;
using System.Data.EntityModel;

namespace WlasnaStrona.Models
{
    public class EntityConcrete : IRepository
    {
        private BazaEntities baza = new BazaEntities();

        public IEnumerable<Table> DaneTablicowe
        {
            get { return baza.Table; }
        }
    }
}