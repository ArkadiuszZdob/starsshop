﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WlasnaStrona.Infrastructure;
using WlasnaStrona.Controllers;
using System.Data.Entity;
using System.Data.EntityModel;

namespace WlasnaStrona.Models
{
    public class StarEntityConcrete : IStarRepository
    {
        private StarEntities bazaGwiazd = new StarEntities();

        public IEnumerable<Stars> SpisGwiazd
        {
            get { return bazaGwiazd.Stars; }
        }

       
    }
}