﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.EntityModel;
using System.Web.Mvc;
using WlasnaStrona.Models;
using WlasnaStrona.Controllers;

namespace WlasnaStrona.Controllers
{
    public class HomeController : Controller
    {
        private IRepository repository;
        private IStarRepository starRepository;

        public HomeController(IRepository repositoryArg,IStarRepository starArg)
        {
            this.repository = repositoryArg;
            this.starRepository = starArg;
        }
        

        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult Sekcja()
        {

            Klasa personalia = (Klasa)Session["Loggowanie"];
            if (personalia != null)
            {
                Klasa login = new Klasa { Login = personalia.Login, Password = personalia.Password };

                return View(login);
            }
                return View();
        }

        [HttpGet]
        public ActionResult Starts()
        {
            ViewBag.Potwierdzenie = "";
                return View();
        }

    [HttpPost]
        public ActionResult Starts(Klasa model)
        {
        
            var AccesConfirmed = from osoba in repository.DaneTablicowe
                                 where ((osoba.Login == model.Login) && (osoba.Password == model.Password))
                                 select new { osoba.Login, osoba.Password };

            int count = AccesConfirmed.Count();

            if (count > 0)
            {
                var Person = AccesConfirmed.First();

                Klasa Dane = new Klasa { Login = Person.Login, Password = Person.Password };

                GetLoggin().Login = Dane.Login;
                GetLoggin().Password = Dane.Password;


                return View("Result", Dane);
            }
            else
            {
                
                return View();

            }
        }

    [HttpGet]
    public ActionResult Rejestracja()
    {
        return View();
    }
   
    [HttpPost]
    public ActionResult Rejestracja(Personalia model)
    {
        BazaEntities baza = new BazaEntities();

        var lookForDoubleName = from log in repository.DaneTablicowe
                                where log.Login == model.Login
                                select new { log.Login };

        bool BothPassTrue = model.Password == model.RepeatPassword ? true : false;

        int count = lookForDoubleName.Count();


        if ((count == 0)&&(BothPassTrue==true)&&(model.Login!=null))
        {
            int noweId = repository.DaneTablicowe.Max(o => o.Id) + 1;
            Table nowa = new Table { Id = noweId, Login = model.Login, Password = model.Password };

            baza.Table.Add(nowa);
            baza.SaveChanges();
            

            Klasa dane = new Klasa { Login = model.Login, Password = model.Password };
            ViewBag.From = "Pomyślna rejestracja, przejdź do strony głównej aby się zalogować.";

            return View("Result",dane);
        }
        else
        {
            
            return View();
        }
    }

    public ActionResult LoggOff()
    {
        if ((Klasa)Session["Loggowanie"] != null)
        {
            Session.Clear();
            ViewBag.Off = "Pomyślnie wylogowano";
            return View("Index");
        }
        else
        {
            ViewBag.Off = "Nie ma nikogo zalogowanego";
            return View("Index");
        }
    }
        

    private Klasa GetLoggin()
    {
        Klasa log = (Klasa)Session["Loggowanie"];
        if (log == null)
        {
            log = new Klasa();
            Session["Loggowanie"] = log;
        }
        return log;
    }

        
    public ActionResult StarShop()
    {
        StarEntities bazaGwiazd = new StarEntities();

        return View(bazaGwiazd);
    }

        
        public ActionResult Rezerwacja(Stars model)
        {
            StarEntities bazaGwiazd = new StarEntities();

            Klasa user = (Klasa)Session["Loggowanie"];

            if (user == null)
            {
                ViewBag.Potwierdzenie = "Zaloguj się aby rezerwować";
            }
            else
            {
                foreach (var g in bazaGwiazd.Stars)
                {
                    if ((g.Nazwa == model.Nazwa))
                    {
                        g.Rezerwacja = user.Login;
                    }
                }
                bazaGwiazd.SaveChanges();
                ViewBag.Potwierdzenie = "Rezerwacja udana";
            }


            
            return View("StarShop",bazaGwiazd);
        }
       


    }
}
